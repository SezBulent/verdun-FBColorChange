// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
function extractHostname(url) {
  var hostname;
  //find & remove protocol (http, ftp, etc.) and get hostname
  if (url.indexOf("://") > -1) {
    hostname = url.split('/')[2];
    console.log('hostname', hostname)
  } else {
    hostname = url.split('/')[0];
  }

  //find & remove port number
  hostname = hostname.split(':')[0];
  //find & remove "?"
  hostname = hostname.split('?')[0];

  return hostname;
}
/**
 * Get the current URL.
 *
 * @param {function(string)} callback called when the URL of the current tab
 *   is found.
 */
function getCurrentTabUrl(callback) {
  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, (tabs) => {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];
    console.log('tabs', tabs)
    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = extractHostname(tab.url);
    console.log('url extract', url)

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, (tabs) => {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

/**
 * Change the background color of the current page.
 *
 * @param {string} color The new background color.
 */
function changeBackgroundColor(color) {

  // + 'document.getElementsByClassName("_2s1y")[0].style.borderColor="' + color + '";';

// THIS FUNCTION EXECUTES IN BROWSER, NOT IN POPUP
  function modifyDOM(color) {
    document.getElementsByClassName("_2s1y")[0].style.backgroundColor = color;
    //document.getElementsByClassName("_2s1y")[0].style.borderColor = color;

//Add the inverted color of background, to text
    div = document.getElementsByClassName("_2s1y")[0];
    console.log("Proprietale al elem:", window.getComputedStyle(div, null).getPropertyValue('background-color'))

    var rgb = window.getComputedStyle(div, null).getPropertyValue('background-color');

    var colors = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    console.log("rgb.match", colors)

//for icon
    console.log("rgb.match", colors[0] > "rgb(200, 200, 200)")
    if (colors[0] == "rgb(255, 255, 255)") {
      document.getElementsByClassName("_2md")[0].style.backgroundColor = "#000000";
      document.getElementsByClassName("_2md")[0].style.borderRadius = "30%";
    } else {
      document.getElementsByClassName("_2md")[0].style.backgroundColor = "unset";
      document.getElementsByClassName("_2md")[0].style.borderRadius = "0";
    }

    //var brightness = 10;

    var r = colors[1];
    var g = colors[2];
    var b = colors[3];

    var rb = Math.round(colors[1] - (colors[1]*15)/100);
    var gb = Math.round(colors[2] - (colors[2]*15)/100);
    var bb = Math.round(colors[3] - (colors[3]*15)/100);

    document.getElementsByClassName("_2s1y")[0].style.borderColor = 'rgb('+rb+','+gb+','+bb+')';
    document.getElementsByClassName("_585-")[0].style.borderColor = 'rgb('+rb+','+gb+','+bb+')';

    console.log("Rgb inchis:", 'rgb('+rb+','+gb+','+bb+')');
    //var ir = Math.floor((255-r)*brightness);
    //var ig = Math.floor((255-g)*brightness);
    //var ib = Math.floor((255-b)*brightness);

    if (r > 175 || g > 175 || b > 175 ) {
      document.getElementsByClassName("_2s1y")[0].style.color = "#000000";

    } else {
      document.getElementsByClassName("_2s1y")[0].style.color = "#ffffff";
    }
    //document.getElementsByClassName("_2s1y")[0].style.color = 'rgb('+ir+','+ig+','+ib+')';

  }
  // See https://developer.chrome.com/extensions/tabs#method-executeScript.
  // chrome.tabs.executeScript allows us to programmatically inject JavaScript
  // into a page. Since we omit the optional first argument "tabId", the script
  // is inserted into the active tab of the current window, which serves as the
  // default.
  chrome.tabs.executeScript({
    code: '(' + modifyDOM.toString() + ')("' + color + '");'
  });
  // chrome.tabs.executeScript({
  //   code: magie2
  // });
}

/**
 * Gets the saved background color for url.
 *
 * @param {string} url URL whose background color is to be retrieved.
 * @param {function(string)} callback called with the saved background color for
 *     the given url on success, or a falsy value if no color is retrieved.
 */
function getSavedBackgroundColor(url, callback) {
  // See https://developer.chrome.com/apps/storage#type-StorageArea. We check
  // for chrome.runtime.lastError to ensure correctness even when the API call
  // fails.
  chrome.storage.sync.get(url, (items) => {
    callback(chrome.runtime.lastError ? null : items[url]);
  });
}

/**
 * Sets the given background color for url.
 *
 * @param {string} url URL for which background color is to be saved.
 * @param {string} color The background color to be saved.
 */
function saveBackgroundColor(url, color) {
  var items = {};
  items[url] = color;
  // See https://developer.chrome.com/apps/storage#type-StorageArea. We omit the
  // optional callback since we don't need to perform any action once the
  // background color is saved.
  chrome.storage.sync.set(items);
}

// This extension loads the saved background color for the current tab if one
// exists. The user can select a new background color from the dropdown for the
// current page, and it will be saved as part of the extension's isolated
// storage. The chrome.storage API is used for this purpose. This is different
// from the window.localStorage API, which is synchronous and stores data bound
// to a document's origin. Also, using chrome.storage.sync instead of
// chrome.storage.local allows the extension data to be synced across multiple
// user devices.

//THIS FUNCTION EXECUTES IN POPUP
document.addEventListener('DOMContentLoaded', () => {
  console.log('DOMLOAD', 'DOMContentLoaded');
  getCurrentTabUrl((url) => {
    var colorchange = document.getElementsByClassName('colorchange')[0];

    // Load the saved background color for this page and modify the dropdown
    // value, if needed.
    getSavedBackgroundColor(url, (savedColor) => {
      if (savedColor) {
        changeBackgroundColor(savedColor);
        colorchange.value = savedColor;
      }
    });

    // Ensure the background color is changed and saved when the dropdown
    // selection changes.
    colorchange.addEventListener('change', () => {
      changeBackgroundColor( '#' + colorchange.value);
      console.log("colorchange.addEventListener", url, '#' + colorchange.value);
      saveBackgroundColor(url, '#' + colorchange.value);

    });
  });
});
